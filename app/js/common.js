
$(function() {

    $('#colorsSelector .colorItem').on('click', function () {
        var imgPath;
        imgPath = $(this).attr('data-img-path');
        console.log(imgPath);
        $('#imgHolder img').attr('src', imgPath);
    });

    var modelSpecs,

        modelPrice,

        modelSpecsHolder,

        modelPriceHolder;

    modelSpecsHolder = $('#modelSpecs');
    modelPriceHolder = $('#modelPrice');

    modelPrice = 0;
    modelSpecs = '';

    function calculatePrice(){

        var modelPriceEngine = $('input[name=engine]:checked', '#autoForm').val();
        var modelPriceTransmission = $('input[name=transmission]:checked', '#autoForm').val();
        var modelPricePackage = $('input[name=package]:checked', '#autoForm').val();

        modelPriceEngine = parseInt(modelPriceEngine );
        modelPriceTransmission = parseInt(modelPriceTransmission);
        modelPricePackage = parseInt(modelPricePackage);

        modelPrice = modelPriceEngine + modelPriceTransmission + modelPricePackage ;
        // alert(modelPrice);

        modelPriceHolder.text(modelPrice + '$');
    };

    function compileSpecs () {
      modelSpecs = $('input[name=engine]:checked + label', '#autoForm').text();
      modelSpecs = modelSpecs + $('input[name=transmission]:checked + label', '#autoForm').text();
      modelSpecs = modelSpecs + $('input[name=package]:checked + label', '#autoForm').text();

      modelSpecsHolder.text(modelSpecs);

    };

    $('#autoForm input').on('change', function () {
        calculatePrice();
        compileSpecs();
    });

    calculatePrice();
    compileSpecs();


	$('#callback-form').validate({
		rules: {
			name: {
				required: true,
				minlength: 3,
				maxlength: 16,
			},
			phone: {
				required: true,
				minlength: 18,
				maxlength: 18
			},
			company: {
				required: true,
				minlength: 3
			},
		},
		messages: {
			name: {
				required: 'Имя обязательно должно быть заполнено',
				minlength: 'Имя должен содержать от 3 до 12 символов',
				maxlength: 'Имя должен содержать от 3 до 12 символов'
			},
			phone: {
				required: 'Телефон обязательно должно быть заполнено',
				minlength: 'Неверный формат номера телефона',
				maxlength: 'Неверный формат номера телефона'
			},
			company: {
				required: 'Организация обязательно должно быть заполнено',
				minlength: 'Поля организация должен содержать от 3 символов',
			},
		},
		submitHandler: function() {
			var th = $('#callback-form');
			$.ajax({
				type: "POST",
			url: "/mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
		}
	});


	$('#phone').mask('+0 (000) 000-00-00',{placeholder: "+ (   )   -  -  "});

	$('a[href*="#callback"]').magnificPopup({
		type: 'inline',
		removalDelay: 500,
    callbacks: {
    	beforeOpen: function() {
    		this.st.mainClass = this.st.el.attr('data-effect');
    	}
    },
    midClick: true,
    closeBtnInside: true,
    preloader: false,
 
	});

	$(".mnu-services-list .mnu-services-inner-wrapper").hide().prev().click(function() {
		$(this).parents(".mnu-services-list").find(".mnu-services-inner-wrapper").not(this).slideUp().prev().removeClass("active");
		$(this).next().not(":visible").slideDown().prev().addClass("active");
	});



	var lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy-load"
    
  });




	var containerEl = document.querySelector('.portfolio-items-wrapper');
	var mixer;

	if (containerEl) {
		mixer = mixitup(containerEl, {
		});
	};



	$('.main-nav').mCustomScrollbar({
		theme: "minimal-dark",
		scrollInertia: 200

	});

	$('body').on('click', function(event) {
		$('.hamburger').removeClass('is-active');
		$('.overlay').removeClass('overlay-active');
		$('.main-nav').removeClass('main-nav-active');
		setTimeout(function(){$('.main-nav-effect').removeClass('main-nav-active-2')}, 50);
		$('.content').removeClass('content-active')
	});

	$('.main-nav, .main-nav-effect').on('click', function(event) {
		event.stopPropagation();
	});

	$('.hamburger').on('click', function(event) {

		event.stopPropagation();

		$(this).hasClass('is-active');

		if(!$('.hamburger').hasClass('is-active')) {
			$('.hamburger').addClass('is-active');
			$('.overlay').addClass('overlay-active');
			$('.main-nav-effect').addClass('main-nav-active-2');
			setTimeout(function(){$('.main-nav').addClass('main-nav-active')}, 150);
			$('.content').addClass('content-active');

		}
		else {
			$('.hamburger').removeClass('is-active');
			$('.overlay').removeClass('overlay-active');
			$('.main-nav').removeClass('main-nav-active');
			setTimeout(function(){$('.main-nav-effect').removeClass('main-nav-active-2')}, 50);
			$('.content').removeClass('content-active')

		}


	});






});

